Gitlab Runner - Salt formula
-----------------------------------------------

This `salt` formula automates the installation of a Gitlab Runner, where runner runs inside docker containers.

The target of this formula are Debian Stretch VMs on Google Compute Engine (GCE). Those VMs have the `stretch-backports` repository enabled by default. Remember to allow http and https traffic during the creation of the VMs on GCE web UI.

Some config files are need to properly deploy the `runner`: `top.sls` and `pillar/top.sls` contains what will be executed in each VM, and `pillar/runner.sls` contains customizable variables. In the repository you can find examples of all these files, all you need to do is rename those files removing the `.example` extension of their names. Detailed information of each of these files will be presented below.

The `top.sls` file contains the hostname of the VMs managed by salt minions and which salt states will be applied to each VM. This file is presente below:

```
base:
  '<runner_hostname>':
    - common
    - runner.docker
    - runner
```

The `runner` VM should be managed by a salt minion, and its key should be accepted by the salt master.

The `pillar/top.sls` file describes which config file will be available to each VM:

```
base:
  '<runner_hostname>':
    - runner
```

Now let's see what is needed in the config files under `pillar` directory. This is the content of the `pillar/runner.sls`:

```
runner:
  config:
    config_path: <config_path>
    docker_socket: <docker_socket>
    base_docker_image: <docker_image>
    gitlab_url: <gitlab_url>
    gitlab_domain: <gitlab_domain>
    registration_token: <token>
```

Below is the description of the variable:

* `config_path`: The path of the directory containing configuration files. By default is used `/etc/gitlab-runner`
* `docker_socket`: The path to the docker socket file. By default is used `/var/run/docker.sock`
* `base_docker_image`: Name of the docker image used by default in the runner.
* `gitlab_url`: URL of the Gitlab Instance. You can find it via web UI in the Runners session.
* `gitlab_domain`: Domain name used by the Gitlab Instance.
* `registration_token`: Token used to register new runners. You can find it via web UI in the Runners session.

We also need the `<gitlab_domain>.crt` file used by the Gitlab server which will be used by the runner to trust on the server. You should copy it from the server and paste it in `agent/config` as `<gitlab_domain>.crt`.

With those files in place we can apply the salt states via the salt master. Again, at this point you should have the salt master and minions already configured, where salt master can communicate with minions without problems.

Do not forget to edit the `/etc/salt/master` config file to point to the right path. `file_roots` should point to the root of this repository and `pillar_roots` should point to the `pillar` directory inside this repository.

Finally, to deploy the Gitlab Runner you need to run the following command from the salt master:

```
# salt '<runner_hostname>' state.apply
```
