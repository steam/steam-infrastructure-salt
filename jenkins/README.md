Jenkins - Agent - Salt formula
-----------------------------------------------

This `salt` formula automate the installation of `jenkins` agent.

The target of this formula are Debian Stretch VMs on Google Compute Engine (GCE). Those VMs have the `stretch-backports` repository enabled by default. Remember to allow http and https traffic during the creation of the VMs on GCE web UI.

`jenkins` is divided in two components mainly:

* `master`: runs the web UI (this formula does not set up the SSL certificate) and all the `jenkins` stuff.
* `agent`: where the jobs will be executed. In this formula we use SSH to connect `master` and `agent`, and we install the Java runtime, docker, and configure `kvm` in the agent.

Only the `agent` is deployed in this formula, keep in mind that it still need some manual configuration to be done via the `master` web UI.

Some config files are need to properly deploy `jenkins agent`: `top.sls` contains what will be executed in each VM, and the file `agent/config/master.id_rsa.pub` should contain the `master`'s SSH public key. In the repository you can find an example of `top.sls` file, all you need to do is rename the file removing the `.example` extension from its name. Detailed information of this file will be presented below.

The `top.sls` file contains the hostname of the VMs managed by salt minions and which salt states will be applied to each VM:

```
base:
  '<agent_hostname>':
    - common
    - agent
    - agent.docker
```

The `agent` VM should be managed by salt minions, and their keys should be accepted by the salt master.

With this file in place we can apply the salt states via the salt master. Again, at this point you should have the salt master and minions already configured, where salt master can communicate with minions without problems.

Do not forget to edit the `/etc/salt/master` config file to point to the right path. `file_roots` should point to the root of this repository.

Remember that the `master`'s SSH public key is needed to allow SSH connections without password, so create the file `agent/config/master.id_rsa.pub` and place the SSH public key in there. After that run the following command to apply the salt states:

```
# salt '<agent_hostname>' state.apply
```

Again, do not forget to do the properly setup via the web UI.
