apt-transport-https:
  pkg.installed

docker_repo:
  pkgrepo.managed:
    - humanname: Docker apt repository
    - name: deb [arch=amd64] https://download.docker.com/linux/debian stretch stable
    - file: /etc/apt/sources.list.d/docker.list
    - key_url: https://download.docker.com/linux/debian/gpg

docker-ce:
  pkg:
    - installed
    - refresh: True
    - require:
      - pkgrepo: docker_repo
