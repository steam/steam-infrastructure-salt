default-jre:
  pkg.installed

git:
  pkg.installed

jenkins:
  user.present:
    - fullname: Jenkins user
    - shell: /bin/bash
    - home: /home/jenkins
    - groups:
      - sudo

ssh_key_master:
  ssh_auth.present:
    - user: jenkins
    - source: salt://agent/config/master.id_rsa.pub

/etc/sudoers.d/sudonopasswd:
  file.managed:
    - source: salt://agent/config/sudonopasswd
    - user: root
    - group: root
    - mode: 440

/etc/udev/rules.d/70local-kvm.rules:
  file.managed:
    - source: salt://agent/config/70local-kvm.rules
    - user: root
    - group: root
    - mode: 440
