/etc/motd:
  file.managed:
    - mode: 0755
    - contents: |
      Managed by Salt, please deploy changes via Salt rather than directly

uptodate:
  pkg.uptodate:
    - refresh: True
