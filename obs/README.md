Open Build Service - Workers - Salt formula
-----------------------------------------------

This `salt` formula automate the installation of the `open-build-service` (`OBS`) workers.

The target of this playbook are Debian Stretch VMs on Google Compute Engine (GCE). Those VMs have the `stretch-backports` repository enabled by default. Remember to allow http and https traffic during the creation of the VMs on GCE web UI.

The `open-build-platform` is divided in three components/packages mainly:

* Web UI/API (`obs-api`)
* Server (`obs-server`)
* Worker (`obs-server`)

This formula does the provision of the Worker in a VM. It can be applied to multiple workers VMs.

Some config files are need to properly deploy `OBS`: `top.sls` and `pillar/top.sls` contains what will be executed in each VM, and `pillar/worker.sls` contains customizable variables. In the repository you can find examples of all these files, all you need to do is rename those files removing the `.example` extension of their names. Detailed information of each of these files will be presented below.

The `top.sls` file contains the hostname of the VMs managed by salt minions and which salt states will be applied to each VM:

```
base:
  '<worker_vm_hostname>':
    - common
    - worker
```

The `worker` VM should be managed by salt minions, and its key should be accepted by the salt master.

The `pillar/top.sls` file describes which config file will be available to each VM:

```
base:
  '<worker_vm_hostname>':
    - worker
```

Now let's see what is needed in the config file under `pillar` directory. The only config file is `pillar/worker.sls`:

```
worker:
  config:
    server_internal_domain: <server_domain>
    number_of_workers: <number_of_workers>
```

Below is the description of each variable:

* `server_internal_domain`: The internal domain name used to call the Server services. Apparently services running on non-standard ports (`5252` and `5352` for instance) are not exposed via the public IP address, and there are no configuration to allow that. During the tests the internal IP address of the `server` VM has been used here.
* `number_of_workers`: The number of workers that will be executed in the `worker` VM. During the tests `1` worker has been used.


With those files in place we can apply the salt states via the salt master. Again, at this point you should have the salt master and minions already configured, where salt master can communicate with minions without problems.

Do not forget to edit the `/etc/salt/master` config file to point to the right path. `file_roots` should point to the root of this repository and `pillar_roots` should point to the `pillar` directory inside this repository.

Finally, let's provision the `worker` VMs. To do that execute the following command to apply salt states:

```
# salt 'worker' state.apply
```

In the end the worker will be connected to the `obs-server` installed in the server VM.
